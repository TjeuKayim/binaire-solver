package main

import (
	"fmt"
	"testing"
)

func main() {
	fmt.Println("Hello World")

	bin := NewBinaire()
	mockBinaire(bin)

	fmt.Print(bin)

	fmt.Println("Should be false: ", bin.validateMove(1, 2, 0))
	fmt.Println("Should be false: ", bin.validateMove(1, 9, 2))
	fmt.Println("Should be true:  ", bin.validateMove(1, 2, 2))

	bin.solve()
	fmt.Print(bin)

	fmt.Println("Should be false: ", bin.validateMove(0, 1, 9))
}

func BenchmarkBinaire(b *testing.B) {
	for i := 0; i < b.N; i++ {
		bin := new(Binaire)
		mockBinaire(bin)
		bin.solve()
	}
}

// Element is one, zero or empty (void)
type Element int8

func (el Element) String() string {
	if el == void {
		return " "
	}
	return fmt.Sprint(int8(el))
}
func (el Element) isValid() bool {
	return el == 0 || el == 1
}

const void = Element(-1)
const binaireSize = 10

// Binaire is a puzzle-game
//  X ->
// Y
// ↓
type Binaire struct {
	matrix [binaireSize][binaireSize]Element
	// Stores the number of ones, zeros and the total number
	rowSum [binaireSize][3]byte
	colSum [binaireSize][3]byte
}

func (bin *Binaire) set(el Element, x, y int) {
	bin.matrix[y][x] = el
	bin.count(x, y)
}

func (bin *Binaire) get(x, y int) Element {
	return bin.matrix[y][x]
}

func (bin *Binaire) solve() {
	fmt.Println("Solving...")
	for {
		success, err := bin.findSimple()
		if err != nil {
			//fmt.Println("Invalid Binaire")
			return
		}
		if success {
			//fmt.Println("Simple")
			continue
		}
		if bin.findComplex() {
			//fmt.Println("Complex")
			continue
		}
		return
	}
}

type solveError struct{}

func (solveError) Error() string {
	return "Binaire solve error"
}

func (bin *Binaire) findSimple() (bool, error) {
	for x := 0; x < binaireSize; x++ {
		for y := 0; y < binaireSize; y++ {
			if bin.get(x, y) != void {
				continue
			}
			// Empty cel
			for el := Element(0); el < 2; el++ {
				if !bin.validateMove(el, x, y) {
					// Invalid move, so insert opposite
					move := el ^ 1
					if !bin.validateMove(move, x, y) {
						return false, new(solveError)
					}
					bin.set(move, x, y)
					return true, nil
				}
			}
		}
	}
	return false, nil
}

func (bin *Binaire) findComplex() bool {
	// Trial and error
	for x := 0; x < binaireSize; x++ {
		for y := 0; y < binaireSize; y++ {
			if bin.get(x, y) != void {
				continue
			}
			// Empty cel
			for el := Element(0); el < 2; el++ {
				next := *bin
				next.set(el, x, y)
				for success := true; success; {
					var err error
					success, err = next.findSimple()
					if err != nil {
						// Yes, found one
						bin.set(el^1, x, y)
						return true
					}
				}
			}
		}
	}
	return false
}

func (bin *Binaire) validateMove(el Element, x, y int) bool {
	// Rule 1: Each column and row has an equal number of ones and zeros
	// colSum := bin.colSum[x][el]
	// rowSum := bin.rowSum[y][el]
	if bin.colSum[x][el] == binaireSize/2 {
		return false
	}
	if bin.rowSum[y][el] == binaireSize/2 {
		return false
	}

	// Rule 2: Do not position more than 2 ones or zeros next to each other
	// Check for neighbours in row
	if !validateNeighbours(x, func(n int) bool { return bin.get(n, y) == el }) {
		return false
	}
	// Check for neighbours in column
	if !validateNeighbours(y, func(n int) bool { return bin.get(x, n) == el }) {
		return false
	}

	// Rule 3: Each row and column is unique
	if bin.colSum[x][2] == binaireSize-1 {
		// This row/col is almost full, compare with other full ones
	loopcolumns:
		for col := 0; col < binaireSize; col++ {
			if bin.colSum[col][2] != binaireSize {
				continue
			}
			// Compare
			for n := 0; n < binaireSize; n++ {
				if n != y && bin.get(x, n) != bin.get(col, n) {
					continue loopcolumns
				}
			}
			// They're equal
			//fmt.Println("Rule 3")
			return false
		}
	}
	if bin.rowSum[y][2] == binaireSize-1 {
		// This row/col is almost full, compare with other full ones
	looprows:
		for row := 0; row < binaireSize; row++ {
			if bin.rowSum[row][2] != binaireSize {
				continue
			}
			// Compare
			for n := 0; n < binaireSize; n++ {
				if n != x && bin.get(n, y) != bin.get(n, row) {
					continue looprows
				}
			}
			// They're equal
			//fmt.Println("Rule 3")
			return false
		}
	}

	return true
}

func validateNeighbours(position int, isOne func(int) bool) bool {
	var count byte
	var direction = 1
	for i := position + 1; i >= 0; i += direction {
		if i < binaireSize && isOne(i) {
			count++
			if count == 2 {
				return false
			}
		} else if direction == 1 {
			// Jump to other side and reverse
			i = position
			direction = -1
		} else {
			break // Give up
		}
	}
	return true
}

func (bin *Binaire) count(x, y int) {
	colSum := bin.colSum[x][:]
	rowSum := bin.rowSum[y][:]
	// Reset
	for i := range colSum {
		colSum[i] = 0
		rowSum[i] = 0
	}
	// Increase the number of ones and zeros
	for n := 0; n < binaireSize; n++ {
		el := bin.get(x, n)
		if el.isValid() {
			colSum[el]++
			colSum[2]++
		}
		el = bin.get(n, y)
		if el.isValid() {
			rowSum[el]++
			rowSum[2]++
		}
	}
	// TODO: Optimize calculation of total
}

func (bin *Binaire) String() (s string) {
	for _, row := range bin.matrix {
		s += fmt.Sprintln(row)
	}
	return
}

// NewBinaire creates an empty puzzle
func NewBinaire() *Binaire {
	// Fill matrix with void
	m := [binaireSize][binaireSize]Element{}
	for y, row := range m {
		for x := range row {
			m[y][x] = void
		}
	}
	bin := &Binaire{matrix: m}

	// Count all rows and columns
	for n := 0; n < binaireSize; n++ {
		bin.count(n, n)
	}

	return bin
}

func mockBinaire(bin *Binaire) {
	bin.set(1, 0, 0)
	bin.set(1, 1, 0)
	bin.set(1, 9, 0)
	bin.set(0, 6, 1)
	bin.set(1, 9, 1)
	bin.set(0, 4, 2)
	bin.set(1, 5, 2)
	bin.set(1, 8, 3)
	bin.set(1, 4, 4)
	bin.set(1, 5, 4)
	bin.set(0, 7, 4)
	bin.set(0, 1, 5)
	bin.set(0, 4, 6)
	bin.set(1, 6, 6)
	bin.set(1, 0, 7)
	bin.set(1, 0, 8)
	bin.set(1, 9, 8)
	bin.set(1, 3, 9)
}